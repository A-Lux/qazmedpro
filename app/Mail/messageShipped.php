<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Storage;

class messageShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        ->attach(public_path('pdf/sample.pdf'), [
//        'as' => 'sample.pdf',
//        'mime' => 'application/pdf',
//    ]);
        $files = null;
        if (isset($this->data->files)) {
            $files = $this->data->file('files')->storeAs('logos', Str::random(10) . '.' . $this->data->file('files')->extension());
        }
        $this->data = $this->data->all();

        $headbox = $this->data["heading"];
        unset($this->data["heading"]);
        unset($this->data["_token"]);
        unset($this->data["_token"]);
        $reutn_fis = $this->from(env("MAIL_USERNAME"))->subject($headbox)
            ->markdown('mail.message')->with("data", $this->data);

        if (!is_null($reutn_fis)) {
            $fil_name=explode("/",$files);
            $mimetype = Storage::mimeType($files);
            $reutn_fis->attach(storage_path("app/".$files), [
                'as' => end($fil_name),
                'mime' => $mimetype,
            ]);
        }
        return $reutn_fis;
    }
}
