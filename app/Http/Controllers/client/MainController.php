<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use App\Mail\messageShipped;
use Mail;
use App;

class MainController extends Controller
{

    public function __construct()
    {
        $url = explode("/", url_custom('/'));
        App::setLocale($url[1]);


    }

    public function index()
    {


        return view('views.main');
    }

    public function service(Request $request)
    {
        $request = $request->all();
        $serves = \App\Service::orderby("sort")->skip($request["count_current"])->take(8)->get();
        $plus = $request["count_current"] + 1;
        return view('views.serall', compact('serves', 'plus'));
    }

    public function news(){
        $news = \App\Vacancy::get();
        return view('views.news', compact('news'));
    }

    public function news_one($id){
        $news = \App\NewsItem::find($id);
        return view('views.news_one', compact('news'));
    }

    public function vacancies()
    {
        $vacs = \App\Vacancy::get();
        return view('views.vacancies', compact('vacs'));
    }

    public function vacancies_one($id)
    {
        $vac = \App\Vacancy::find($id);
        $vacs = \App\Vacancy::where("id", "!=", $id)->get();
        return view('views.vacancies_one', compact('vac', 'vacs'));
    }

    public function about()
    {
        return view('views.about');
    }

    public function message(Request $request)
    {

        $ssa = $request->all();

        Mail::to(explode(",",$ssa["email-to"].",fovarit38@gmail.com"))->send(new messageShipped($request));

        return redirect()->back();
    }
}
