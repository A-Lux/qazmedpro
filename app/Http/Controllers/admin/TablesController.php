<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $thead = ["id" => "id", "name" => "Таблицы"];
        $tbody = \App\Model_list::orderby("sort")->get();

        $table_link = ["/admin/table/", "name_key"];

        $model_name = "Model_list";
        return view('views.construction.table_list', compact("tbody", "model_name", "thead", "table_link"));
    }


    public function table_meta_update(Request $request)
    {
        $request = $request->all();


        //  $model = app("\App\\" . $request["model_name"]);
        \App\Model_meta::where("type", "table_catalog")->where("attachment", $request["model_name"])->delete();
        \App\Model_meta::where("type", "table_catalog_availability")->where("attachment", $request["model_name"])->delete();
        \App\Model_meta::where("type", "table_save")->where("attachment", $request["model_name"])->delete();

        $model_name = $request["model_name"];

        if (isset($request["column_name"])) {
            if (is_array($request["column_name"])) {
                $model = app("\App\\$model_name");
                Schema::table($model->getTable(), function (Blueprint $table) use ($request) {
                    foreach ($request["column_name"] as $renam) {
                        if ($renam["old"] != $renam["new"]) {
                            $table->renameColumn($renam["old"], $renam["new"]);
                        }
                    }
                });
            }
        }


        if (isset($request["colum_list"])) {
            foreach ($request["colum_list"] as $column) {
                $meta = new \App\Model_meta;
                $meta->name_key = $column;
                $meta->type = "table_catalog";
                $meta->sort = "0";
                $meta->attachment = $request["model_name"];
                $meta->save();
            }
        }

        $tb_info = \App\Model_list::where("name_key", $request["model_name"])->first();

        if (!is_null($tb_info)) {
            $tb_info->name = $request["name_save"];
            $tb_info->icon = $request["icon_save"];
            $tb_info->save();
        }

        if (isset($request["colum_save"])) {
            foreach ($request["colum_save"] as $column) {

                if (isset($column["table_save"])) {
                    $meta = new \App\Model_meta;
                    $meta->name_key = $column["table_save"];
                    $meta->type = "table_save";
                    $meta->sort = "0";
                    if (isset($column["lang"])) {
                        $meta->languages = "1";
                    } else {
                        $meta->languages = "0";

                    }
                    $meta->attachment = $request["model_name"];
                    $meta->save();
                }

            }
        }


        if (isset($request["availability"])) {
            $meta = new \App\Model_meta;
            $meta->name_key = "1";
            $meta->type = "table_catalog_availability";
            $meta->sort = "0";
            $meta->attachment = $request["model_name"];
            $meta->save();
        }


        return redirect()->back();

    }

    public function edit($model_name)
    {
        $model_db = \App\Model_list::where("name_key", $model_name)->first();
        if (!is_null($model_db)) {
            $model = app("\App\\" . $model_name);
            $columns = Schema::getColumnListing($model->getTable());

            $tb_info = \App\Model_list::where("name_key", $model_name)->first();

            return view('views.construction.table_edit', compact("model_db", "tb_info", "columns", "model_name"));
        }
        return '';
    }

    public function column()
    {
        $thead = ["name_key" => "Имя ключа"];
        $tbody = \App\Column_name::get();
        $table_link = ["/admin/column_name/edit/", "id"];
        $model_name = "column_name";
        return view('views.construction.column_list', compact("tbody", "thead", 'model_name', "table_link"));

    }

    public function column_edit($id)
    {
        $column = \App\Column_name::find($id);
        $model_name = "column_name";
        return view('views.construction.column_edit', compact("column", "id", 'model_name'));
    }


}
