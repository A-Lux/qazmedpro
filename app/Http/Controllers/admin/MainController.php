<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;
use TableClass;
use Str;
class MainController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        return view('views.main');
    }


    public function table()
    {
        return view('main');
    }


    public function developer($status)
    {
        if ($status != "true") {
            $status = false;
        }
        Cache::forever('dev', $status);



        return redirect()->back();
    }


    public function orders(){
        return view('views.orders');
    }



    public function s_text()
    {
        $model_name = "StaticText";
        $thead_nav = \App\StaticText::get()->groupby("page");
        $tbody = [];

        $page = '';
        if (isset($_REQUEST["page"])) {
            $tbody = \App\StaticText::where("page", $_REQUEST["page"])->get();
            $page = $_REQUEST["page"];
        }

        $thead = ["id" => "id", "name_key" => "name_key"];

        $table_link = ["/admin/model//", "id"];

        return view('views.s_text', compact("model_name", 'page', "thead_nav", "tbody", "thead", "table_link"));

    }



    public function update_text(Request $request)
    {
        $files = $request;
        $request = $request->all();
        $request = $request["save"];

        if (isset($_FILES["save"])) {
            foreach ($_FILES["save"]["name"] as $nameInput => $fileout) {
                if (isset($request[$nameInput])) {
                    if ($files->hasFile("save." . $nameInput)) {
                        $photo_path = $files->file("save." . $nameInput);
                        $type = explode(".", $photo_path->getClientOriginalName());
                        if (count($type) > 1) {
                            $m_path = Str::random(5) . "_file." . $type[1];
                            $moveTo = 'media/Update/' . str_replace("/", "_", $_FILES["save"]["type"][$nameInput]) . '/';
                            $photo_path->move($moveTo, $m_path);
                            $request[$nameInput] = "/public/" . $moveTo . $m_path;

                        }
                    }
                }
            }
        }

        foreach ($request as $key => $colum) {
            $s_text = \App\StaticText::where("name_key", $key)->first();
            if (is_array($colum)) {
                $s_text->content = json_encode($colum, JSON_UNESCAPED_UNICODE);
            } else {

                $s_text->content = $colum;

            }
            $s_text->save();
        }
        return redirect()->back();
    }
}
