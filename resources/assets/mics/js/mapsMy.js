"use strict";

export function mapinit() {

    var
        _this = this;
    this.yanload = false;
    this.myCollection = [];
    this.init = function (maps) {

    };
    this.metro = function (maps) {

    };
    this.mapsList = [];

    try {
        if (typeof ymaps != "undefined") {
            this.init = function init(maps, coors) {
                if (typeof coors == "undefined") {
                    coors = [43.252835, 76.914831];
                } else {
                    coors[0] = parseFloat(coors[0]);
                    coors[1] = parseFloat(coors[1]);
                }
                // Создание карты.
                ymaps.ready(function () {
                    if ($("*").is("#" + maps)) {
                        if (typeof _this.mapsList[maps] == "undefined") {
                            _this.mapsList[maps] = (new ymaps.Map(maps, {
                                center: coors,
                                zoom: 13,
                                controls: [],
                            }));
                            _this.myCollection[maps] = new ymaps.GeoObjectCollection();
                            return "200"
                        } else {
                            return "501"
                        }
                    } else {
                        return "404";
                    }
                });
            }

            this.setCenter = function setCenter(maps, cor, zoom) {
                if (typeof zoom == "undefined") {
                    zoom = 13;
                }
                ymaps.ready(function () {
                    cor[0] = parseFloat(cor[0]);
                    cor[1] = parseFloat(cor[1]);
                    if (typeof _this.mapsList[maps] != "undefined") {
                        // _this.mapsList[maps].setType('yandex#hybrid');
                        _this.mapsList[maps].panTo(cor, {
                            delay: 1500,
                        });
                        setTimeout(function () {
                            _this.mapsList[maps].setZoom(zoom, {duration: 300});
                        }, 600);

                    }
                });
            }

            this.addPoint = function addPoint(maps, settingsUser, presetUser) {

                if (typeof _this.mapsList[maps] != "undefined") {
                    if (typeof settingsUser == "undefined") {
                        settingsUser = {};
                    }

                    var
                        settingsInit = {
                            "type": "Point",
                            "coordinates": [43.258355, 76.920410],
                            "iconContent": "",
                            "hintContent": "",
                            "preset": "islands#blackStretchyIcon",
                            "draggable": true,
                        };
                    if (typeof presetUser != "undefined") {
                        settingsInit["preset"] = presetUser;
                    }
                    if (!jQuery.isPlainObject(settingsUser)) {
                        if (jQuery.isArray(settingsUser)) {
                            settingsUser[0] = parseFloat(settingsUser[0]);
                            settingsUser[1] = parseFloat(settingsUser[1]);
                            settingsUser = {"coordinates": settingsUser};
                        }
                    }
                    var settings = $.extend(settingsInit, settingsUser);

                    ymaps.ready(function () {
                        var Markdata = new ymaps.GeoObject({
                            geometry: {
                                type: settings.type,
                                coordinates: settings.coordinates
                            },
                            properties: {
                                iconContent: settings.iconContent,
                                hintContent: settings.hintContent
                            }
                        }, {
                            preset: settings.preset,
                            draggable: settings.draggable
                        });

                        _this.myCollection[maps].add(Markdata);

                        _this.mapsList[maps].geoObjects.add(_this.myCollection[maps]);


                    });
                }
            }

            this.addPointCustom = function addPoint(maps, settingsUser, images) {


                if (typeof _this.mapsList[maps] != "undefined") {
                    if (typeof settingsUser == "undefined") {
                        settingsUser = {};
                    }


                    var
                        settingsInit = {
                            "balloonContent": "",
                            "hintContent": "",
                            "coordinates": []
                        };

                    if (!jQuery.isPlainObject(settingsUser)) {
                        if (jQuery.isArray(settingsUser)) {
                            settingsUser[0] = parseFloat(settingsUser[0]);
                            settingsUser[1] = parseFloat(settingsUser[1]);
                            settingsUser = {"coordinates": settingsUser};
                        }
                    }
                    var settings = $.extend(settingsInit, settingsUser);

                    ymaps.ready(function () {
                        var Markdata = new ymaps.Placemark(settingsUser["coordinates"], {
                            hintContent: settingsInit["hintContent"],
                            balloonContent: settingsInit["balloonContent"]
                        }, {
                            // Опции.
                            // Необходимо указать данный тип макета.
                            iconLayout: 'default#image',
                            // Своё изображение иконки метки.
                            iconImageHref: images,
                            // Размеры метки.
                            iconImageSize: [17, 24],
                            // Смещение левого верхнего угла иконки относительно
                            // её "ножки" (точки привязки).
                            iconImageOffset: [-8, -24]
                        });

                        _this.myCollection[maps].add(Markdata);

                        _this.mapsList[maps].geoObjects.add(_this.myCollection[maps]);


                    });


                }
            }


            this.removeAll = function removeAll(maps) {
                _this.myCollection[maps].removeAll();

            }


            this.type = function type(maps) {
                // Создание карты.

            }

        }
    } catch (e) {
        console.log(e);
    }

}



