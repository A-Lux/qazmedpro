import jquery from 'jquery';

window.$ = window.jQuery = jquery;
require('@fancyapps/fancybox');
require('../../../../node_modules/bootstrap/dist/js/bootstrap.min');
require('../../../../node_modules/bootstrap-select/js/bootstrap-select');
require('../../../../node_modules/node-waves/dist/waves');
require('../../../../node_modules/tablesorter/dist/js/jquery.tablesorter');
require('../../../../node_modules/tablesorter/dist/js/jquery.tablesorter.widgets.min');
require('../../../../node_modules/tablesorter/dist/js/widgets/widget-pager.min');
require('../../../../node_modules/jquery-ui-bundle/jquery-ui');
require('../../../../node_modules/jquery-ui-touch-punch/jquery.ui.touch-punch');
require('./jquery.datepicker.extension.range.min');
require('../../mics/js/app');

// Import TinyMCE
import tinymce from 'tinymce/tinymce';

// Default icons are required for TinyMCE 5.3 or above
import 'tinymce/icons/default';

// A theme is also required
import 'tinymce/themes/silver';

// Any plugins you want to use has to be imported
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullpage';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/lists';

$(function () {

    Waves.attach('.flat-buttons', ['waves-button']);
    Waves.init();


    tinymce.init({
        selector: '.editor',
        menubar: 'insert',
        plugins: ['paste', 'link', 'code','lists advlist'],
        toolbar: 'undo redo | link | ' +
            'removeformat |  Strikethrough  | code | bold | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat',
    });



    window.randerdate = function (date) {
        $(".boxClsendar_redult").html("");
        date.forEach(function (date, index) {
            $(".boxClsendar_redult").append('<div class="text_datebpx"> ' + date.replace(/\//g, '.') + ' </div>');
        });
    }

    if ($("*").is(".multidate")) {

        $('.multidate').each(function () {

            var idName = "data" + Math.floor(Math.random() * 11);
            $(this).prop("id", idName);

            var multitextarray = $('.multipleDate', $__this).val().split(',');

            var $__this = $(this).parent();
            var datapiic = $("#" + idName).datepicker({
                range: 'multiple', // режим - выбор нескольких дат
                setDate: null,
                multidate: true,
                range_multiple_max: '90', // макимальное число выбираемых дат
                onSelect: function (dateText, inst, extensionRange) {
                    $('.multipleDate', $__this).val(extensionRange.datesText.join(','));
                    window.randerdate(extensionRange.datesText);
                }
            });

            var defdays = [];
            $.each(multitextarray, function (key, index) {
                var datearra = index.split("-");
                defdays.push(datearra[1] + '/' + datearra[2] + '/' + datearra[0] );

            });
            if (defdays.length > 0) {
                datapiic.datepicker('setDate', defdays);
            }


            var extensionRange = $(datapiic).datepicker('widget').data('datepickerExtensionRange');
            window.randerdate(extensionRange.datesText);
            if (extensionRange.datesText) $('.multipleDate', $__this).val(extensionRange.datesText.join(','));


        });
    }

    $(document).on("click", ".clickNexts", function () {
        location.href = $(".nextlink", this).prop("href");
    });

    $(document).on("change", ".filecontrols", function () {
        if ($(this).get(0).files.length) {
            $(".boxLImaege", $(this).parent()).addClass("active");
        } else {
            $(".boxLImaege", $(this).parent()).removeClass("active");
        }
    });


    $(document).on("click", ".navbar-brand", function (e) {
        $(".sidebar").addClass("active");
    });

    $(document).on("click", ".lang_control", function (e) {

        var $this = $(this);
        var lang = $this.data("lang");

        $(".lang_control").removeClass("active");
        $(".language_grup_hid").removeClass("active");
        $(".language_grup_multi").addClass("active");
        $(".language_grup_" + lang).addClass("active");
        $this.addClass("active");

        $(".transition").css("transform", "translateX(" + (((100 / $(".lang_control").length) * ($this.index() - 1)) * (-1)) + "%)");


    });


    $(document).on("click", ".cli", function (e) {
        var
            $this = $(this),
            closest = $this.data("closest"),
            closestCli = $this.data("closestcli"),
            unDefain = $this.data("un"),
            target = $this.data("target"),
            $target = $this.data("target");
        if (typeof closest != "undefined") {
            $this = $($this.closest(closest));
        }
        if (typeof unDefain == "undefined") {
            unDefain = 0;
        } else {
            unDefain = 1;
        }
        if (typeof target != "undefined") {
            $target = $(target);
        }
        if (typeof closestCli != "undefined") {
            $(".cli", $this.closest(closestCli)).removeClass("active");
            $this.addClass("active");
        } else if (unDefain == 0) {
            if ($this.hasClass("active")) {
                $this.removeClass("active");
            } else {
                $this.addClass("active");
            }
        } else if (unDefain != 0) {
            $this.addClass("active").siblings().removeClass("active");
            if (typeof target != "undefined") {
                $target.eq($this.index() - 1).addClass("active").siblings().removeClass("active");
            }
        }
    });


    $(".multiselect").selectpicker({
        noneSelectedText: 'Ничего не выбрано'
    });
    $(".selectpicker").selectpicker({
        noneSelectedText: 'Ничего не выбрано'
    });


    $(document).on("click", ".openModel", function () {
        var
            $this = $(this);
        //     $target = $this.data("target"),
        //     $closest = $this.data("closest");
        //
        // if (typeof $closest != "undefined") {
        //     var
        //         $_closest = $($target, $($this.closest($closest)));
        //
        //     if ($_closest.length > 0) {
        //         $target = $_closest;
        //     }
        //
        // }
        //
        // $.fancybox.open($target.html());
        $this.fancybox({
            'type': 'iframe'
        });

        return false;

    });


    $(".form-link").each(function () {

        var
            $this = $(this);

        if ($(".input-line", $this).length == 0) {
            $this.append(" <div class='input-line'></div> ")
        }

    });


    $(".button-logout").on("click", function (event) {
        event.preventDefault();
        document.getElementById('logout-form').submit();
    });

//Table
//     $('.js-table').each(function (key, index) {
//         var
//             $this = $(this);
//         var $table = 'table_' + key;
//
//         $table = $('.js-table').DataTable({
//             // dom: 'Bfrltip',
//             dom: 'Bfrtip',
//             pageLength: 100,
//             // bPaginate: false,
//             buttons: [
//                 'copy', 'csv', 'excel', 'pdf', 'print'
//             ]
//         });
//
//         if (typeof $this.data('order-column') !== 'undefined' && typeof $this.data('order-type') !== 'undefined') {
//             $table.order([$this.data('order-column') - 1, $this.data('order-type')]).draw();
//         }
//     });


    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width());
        });

        return $helper;
    };
    var updateIndex = function (e, ui) {
        $('td.index', ui.item.parent()).each(function (i) {
            var position = i + 1;


            var id = $(this).data('id');

            console.log(id, "", position);

            $.ajax({
                url: '',
                method: 'POST',
                data: {id: id, position: position},
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                },
                error: function (msg) {
                    console.log(msg);
                }
            });
        });

    };


    $(".sortable tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex,
        distance: 35,
        start: function (e, ui) {
            ui.placeholder.height(ui.helper.height());

        }
    });


    $(".sortTables")
        .tablesorter({
            widthFixed: true, widgets: ['zebra', "filter"],
            widgetOptions: {
                // filter_anyMatch replaced! Instead use the filter_external option
                // Set to use a jQuery selector (or jQuery object) pointing to the
                // external filter (column specific or any match)
                filter_external: '.search',
                // add a default type search to the first name column
                filter_defaultFilter: {1: '~{query}'},
                // include column filters
                filter_columnFilters: false,
                filter_placeholder: {search: 'ПОИСК...'},
            }
        });


});
