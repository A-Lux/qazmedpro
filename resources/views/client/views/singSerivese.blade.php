<div class="services-one">
    <div class="services-one_title">
        <p class="text text-s18">
            {{LC($service->name)}}
        </p>
    </div>
    <div class="services-one_text">
        <div class="text text-s14">
            {!! LC($service->content) !!}
        </div>
    </div>
    <div class="services-one_number">
        <p class="text text-s60">
            {{str_pad($v+(isset($plus)?$plus:0),2,"0",STR_PAD_LEFT)}}

        </p>
    </div>
</div>
