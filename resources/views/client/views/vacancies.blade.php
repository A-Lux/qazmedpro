@extends('views.layouts.app')

@section('title')
    {!!Strip_tags(s_("Vacancies Title","Seo оптимизация","",""))!!}
@endsection

@section('description')
{!!  Strip_tags(s_("description vacancies","Seo оптимизация","",""))!!}
@endsection

@section('type')
website
@endsection

@section('ogimage')
https://qazmedpro.kz/graph.png
@endsection

@section('content')


    <section class="slider">
        <div class="prop">
            <div class="prop_img prop_img-35">
                <div class="prop_img_src"
                     style="background-image: url('/public/media/client/images/f891592a-5311-4cd0-bd27-3c7d0074a393.png');">

                </div>
            </div>
        </div>
    </section>

    <section class="vacancies">
        <div class="container vacancies-box">
            <div class="vacancies-box_text">
                <h4 class="after text text-roboto text-s32">
                    {!! s_("Вакансии заголовок","Вакансии","Работа в QazMedPro","") !!}
                </h4>
                <div class="vacancies-content">
                    <div class="vacancies-content_main text text-roboto text-s16">
                        <p></p>
                        {!! s_("Вакансии описание","Вакансии","","textarea") !!}
                    </div>
                </div>
            </div>
            <div class="vacancies-box_list">
                <div class="text text-roboto text-s28" style="color: #014094;">
                    {!! s_("Список вакансий list","Вакансии","Открытые вакансии QazMedPro","textarea") !!}
                </div>
                <div class="vacanci-list">
                    @foreach($vacs as $vac)
                    <a href="{{url_custom('/vacancies/'.$vac->id)}}" class="text text-roboto  text-s20">{!! LC($vac->name) !!} </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <style>
        header.header {
            background-color: #013882;
            position: relative;
        }
        .contacts{
            padding-top: 0;
        }
        .slider:after{
            display: none;
        }
    </style>
@endsection
