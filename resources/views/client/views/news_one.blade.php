@extends('views.layouts.app')
@section('title')
{!! LC($news->name) !!}
@endsection

@section('description')
{!!  Strip_tags(s_("description","Seo оптимизация","",""))!!}
@endsection

@section('type')
article
@endsection

@section('ogimage')
{!! $news->images_single !!}
@endsection

@section('content')




    <section itemscope itemtype="http://schema.org/Article" class="news-bbox">
        <div class="container ">

            <div class="news-bbox_name">
                <h1 class="text text-s32">
                    <b> {!! s_("Заголовок Новости","Новости","Новости","text") !!}</b>
                </h1>
            </div>

            <div class="news_single">
                <div class="news_single_img">
                    <div class="prop">
                        <div class="prop_img" style="padding-top: 23%;">
                            <div title="{!! LC($news->name) !!}" class="prop_img_src"
                                 style="background-image: url(' {!! $news->images_single !!}');background-size: cover;">

                                 <meta itemprop="image" content="{!! $news->images_single !!}"

                            </div>
                        </div>
                    </div>
                </div>
                <div class="news_single_title">
                    <h2 itemprop="name" class="text text-s32">
                        {!! LC($news->name) !!}
                    </h2>
                </div>
                <div class="news_single_data">
                    <time itemprop="datePublished" datetime ="{!! date("d-m-Y",strtotime($news->day)) !!}" class="text text-s20">
                        {!! date("d-m-Y",strtotime($news->day)) !!}
                    </time>
                </div>

                <div itemprop="articleBody" class="news_single_content text text-s16">
                    {!! LC($news->content) !!}
                </div>
            </div>


        </div>
    </section>

    <style>
        header.header {
            background-color: #013882;
            position: relative;
        }

        .contacts {
            padding-top: 0;
        }

        .slider:after {
            display: none;
        }
    </style>
@endsection
