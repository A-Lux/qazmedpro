@extends('views.layouts.app')

@section('title')
    {!!Strip_tags(s_("About Title","Seo оптимизация","",""))!!}
@endsection

@section('description')
{!!  Strip_tags(s_("description about","Seo оптимизация","",""))!!}
@endsection

@section('type')
website
@endsection

@section('ogimage')
https://qazmedpro.kz/graph.png
@endsection

@section('content')


    <section>
        <div class="container">
            <div class="about">
                <div class="about_head">
                    <div class="about_text">
                        <div class="about_text_name text text-roboto text-s32">
                            {!! s_("О компании заголовок","О компании","О нашей компании","") !!}
                        </div>
                        <div class="about_text_content text text-roboto text-s14">
                            {!! s_("О компании описание","О компании","","textarea") !!}
                        </div>
                    </div>
                    <div class="about_img">
                        <div class="prop">
                            <div class="prop_img prop_img-74">
                                <div class="prop_img_src"
                                     style="background-image: url('  {!! s_("О компании v картинка","О компании","/public/media/client/images/pexels-sevenstorm-juhaszimrus-443383 1.png","images") !!}');"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about_main text text-roboto text-s14">
                    {!! s_("О компании второе описание","О компании","","textarea") !!}
                </div>
            </div>
        </div>

    </section>



    <style>
        header.header {
            background-color: #013882;
            position: relative;
        }

        .contacts {
            padding-top: 0;
        }
    </style>
@endsection
