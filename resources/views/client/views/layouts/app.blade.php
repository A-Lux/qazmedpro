<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   {{-- {!!Strip_tags(s_("Title","Seo оптимизация","",""))!!} --}}
   <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/media/client/css/style.css?v=0.8">
    <link rel="stylesheet" href="/media/client/fonts/marianpro">
    <meta name="description" content="@yield('description')">
    <meta name="Keywords" content="{!!  Strip_tags(s_("Keywords","Seo оптимизация","",""))!!}">
    <meta property="og:site_name" content="Qazmedpro.kz">
    <meta property="og:type" content="@yield('type')">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('ogimage')">
    <meta property="og:url" content="{{ url()->current() }}">

    <!-- Bootstrap CSS -->

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="theme-color" content="#ffffff">
    <style>
        .menu {
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 40;
            background-color: #fff;
            visibility: hidden;
            transform: translateX(-100%);
            transition: 0.6s;
            background-color: #013882;
        }
    </style>
</head>
<body>

@php
    $nav=[
        [s_("Кнопка 1","Header",'О компании',""),('/about')],
        [s_("Кнопка 2","Header",'Наши услуги',""),('#services')],
        [s_("Кнопка 3","Header",'Видеопрезентация',""),('#video')],
        [s_("Кнопка 4","Header",'Контакты',""),('#contacts')],
        [s_("Кнопка 6","Header",'Новости',""),('/news')],
        [s_("Кнопка 5","Header",'Вакансии',""),('/vacancies')],
    ];
@endphp
<div class="menu">
    <div class="container">
        <div class="menu_block">

            <a href="javascript:void(0)" class="close">
                <img src="/media/client/images/close.png" alt="">
            </a>


            <a href="/" class="logo logo-menu">
                <img src="/media/client/images/logo.svg" alt="">
            </a>
            <nav class="nav nav-menu">
                <ul>
                    @foreach($nav as $n)
                        <li>
                            <a href="{{url_custom($n[1])}}">  <span
                                    class="text text-s16">{{$n[0]}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
            </nav>

        </div>
    </div>
</div>

<main>
    <header class="header">
        <div class="header_main container">
            <div class="header_main_desk">

                <div class="menu_click">
                    <a href="javascript:void(0)" class="menu_icon menu_open">
                        <div></div>
                        <div></div>
                        <div></div>
                    </a>
                </div>

                <a href="/" class="logo">
                    <img src="/media/client/images/logo.svg" alt="">
                </a>


                <nav class="nav nav-head">
                    <ul>
                        @foreach($nav as $n)
                            <li>
                                <a href="{{url_custom($n[1])}}">  <span
                                        class="text text-s12">{{$n[0]}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>

                <div class="phone phone-dack">
                    <div class="phone_head">
                        <p class="text text-s12">
                            {!! s_("Текст номер телефона","Header",'Номер телефона',"") !!}
                        </p>
                    </div>
                    <div class="phone_tel">
                        <p class="text text-s20">
                            {!! s_("Номер телефона номер","Header",'8 (701) 534 9760',"") !!}
                        </p>
                    </div>
                </div>

                <div class="langs">

                    @php
                        $language_s = App::getLocale();
                        $currenlan=\App\Language::where("name_key",$language_s)->first();
                    @endphp

                    <a href="/" class="lang active">
                        <span class="prop lang_img">
                            <span class="prop_img prop_img-100">
                                <span class="prop_img_src"
                                      style="background-image: url('/media/client/images/lang/{{$language_s}}.png');"></span>
                            </span>
                        </span>
                        <span
                            class="text text-s14">{{$currenlan->name}}</span></a>

                    <div class="drop_lang">
                        @foreach(\App\Language::where("name","!=",$currenlan->name)->get() as $language)
                            <a href="{{url_custom_lang($language->name_key)}}"
                               class="lang ">
                                <span class="prop lang_img">
                                    <span class="prop_img prop_img-100">
                                        <span class="prop_img_src"
                                              style="background-image: url('/media/client/images/lang/{{$language->name_key}}.png');"></span>
                                    </span>
                                </span>
                                <span
                                    class="text text-s14">{{$language->name}}</span></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <style>
        .langs {
            position: relative;
        }

        .drop_lang {
            position: absolute;
            top: 100%;
            visibility: hidden;
            opacity: 0;
            transition: 0.6s;
            padding: 0.5rem 0;
            min-width: 4rem;
        }

        .langs:hover .drop_lang {
            visibility: visible;
            opacity: 1;

        }

        .lang + .lang {
            margin-top: 0.5rem;
        }
    </style>

    @yield('content')


    <div id="contacts" class="contacts">
        <div class="container contacts_pos">
            <div class="contacts_main">
                <div class="contacts_head">
                    <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress" class="services_head ">
                        <h3 class="title text text-s14">
                            {!! s_("Заголовок","Подвал контакты","Контакты") !!}
                        </h3>
                        <h4 class="after text text-s35">

                            {!! s_("Под заголовок","Подвал контакты","Свяжитесь с нами") !!}
                        </h4>
                    </div>
                    <div class="contacts_head_text">
                        <div class="contacts-info">
                            <div class="contacts-info_head">
                                <p class="text text-s16">
                                    {!! s_("Второй заголовок","Подвал контакты","Головной офис:") !!}
                                </p>
                            </div>
                            <div itemprop="streetAddress" class="text text-s16">
                                {!! s_("Адреса","Подвал контакты","г.Алматы, ул.Луганского 54В эл.<br> адрес: reception@qazmedpro.kz","textarea") !!}
                            </div>
                        </div>
                        <div class="contacts-info">
                            <div class="contacts-info_head">
                                <p class="text text-s16">
                                    {!! s_("Третий заголовок","Подвал контакты","Менеджер по развитию бизнеса:","") !!}
                                </p>
                            </div>
                            <div itemprop="telephone" class="text text-s16">
                                {!! s_("Номера/Почта","Подвал контакты","Пасечников Денис <br> моб. 8-701-9303535<br> адрес: d.pasechnikov@qazmedpro.kz","textarea") !!}
                            </div>
                        </div>
                        <div class="contacts-info">
                            <div class="contacts-info_head">
                                <p class="text text-s16">
                                    {!! s_("Черверный заголовок","Подвал контакты","Специалист по персоналу:","") !!}
                                </p>
                            </div>
                            <div class="text text-s16">
                                {!! s_("Номера/Почта вторая","Подвал контакты","адрес: elmira.sultanbekova@qazmedpro.kz<br> моб.8-701-2289655","textarea") !!}
                            </div>
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0)" class="btn btn-slid open-model"><span
                        class="text text-s16">{!! s_("Кнопка Обратная связь","Кнопки","Обратная связь","") !!}</span></a>
            </div>
            <div class="contacts_maps">
                <div class="prop">
                    <div class="prop_img prop_img-54">
                        <div class="prop_img_src"
                             style="background-image: url('/media/client/images/icon/map.png')">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="text text-s14" style="color: #2f3b4c;">
            {!! s_("Копирайт","Подвал","© QazMedPro, 2003 — 2020. Все права защищены.","textarea") !!}
        </div>
    </footer>

</main>


<div class="model model-order">
    <form method="post" action="{{url_custom('/message')}}" class="model_main send-info">
        <div class="model_main_position">
            <div class="model_main_head">
                <h3 class="title text text-s14">
                    <b>{!! s_("Заголовок 1","Форма 1","ЗАПОЛНИТЕ ФОРМУ","") !!}</b>
                </h3>
                <h4 class="after text text-s35">
                    <b>{!! s_("Заголовок 2","Форма 1","Обратной связи","") !!}</b>
                </h4>
                <p class="text text-s14" style="margin-top: 0.5rem;"> {!! s_("Описание","Форма 1","И наш менеджер свяжется с Вами в ближайшее время","") !!}</p>
            </div>
            <input type="hidden" name="email-to" value="marketing@qazmedpro.kz">
            <input type="hidden" name="heading" value="Заявка обратной связи">
            <div class="model_main_input">
                <input type="text" class="text text-s14" required name="name" placeholder="{!! s_("Имя","Форма 1","Имя","") !!}">
                <input type="text" class="text text-s14" required name="phone" placeholder="{!! s_("Номер телефона","Форма 1","Номер телефона","") !!}">
                <textarea name="message" class="text text-s14" placeholder="{!! s_("Ваш запрос","Форма 1","Ваш запрос","") !!}"></textarea>
            </div>
            <div style=" padding: 1.5rem 0; " class="recapcha"></div>
            <div class="model_main_btn">
                <button type="submit" class="btn btn-slid"><span class="text text-s16">{!! s_("Кнопка","Форма 1","Отправить","") !!}</span></button>
            </div>
        </div>

    </form>
</div>

<div itemscope itemtype="https://schema.org/Organization">
<meta itemprop="name" content="qazmedpro">
<link itemprop="url" href="https://qazmedpro.kz/">
<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
<link itemprop="contentUrl url" href="https://qazmedpro.kz/media/client/images/logo.svg">
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/media/client/js/app.js?v=0.39"></script>
{{-- <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(69054700, "init", {
         clickmap:true,
         trackLinks:true,
         accurateTrackBounce:true,
         webvisor:true
    });
 </script>
 <noscript><div><img src="https://mc.yandex.ru/watch/69054700" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
 <!-- /Yandex.Metrika counter --> --}}
</body>
</html>
