@extends('views.layouts.app')

@section('title')
    {!!Strip_tags(s_("News Title","Seo оптимизация","",""))!!}
@endsection

@section('description')
{!!  Strip_tags(s_("description news","Seo оптимизация","",""))!!}
@endsection

@section('type')
website
@endsection

@section('ogimage')
https://qazmedpro.kz/graph.png
@endsection

@section('content')




    <section class="news-bbox">
        <div class="container ">

            <h1 class="text text-s32">
                 {!! s_("Заголовок Новости","Новости","Новости","text") !!}
            </h1>

            <div class="news-flex">
                @foreach(\App\NewsItem::orderby("day","desc")->get() as $ns)
                    <a href="{{url_custom('/news/'.$ns->id)}}" class="news-item">
                        <div class="news-item_img">
                            <div class="prop">
                                <div class="prop_img prop_img-62">
                                    <div title=" {!! LC($ns->name) !!}" class="prop_img_src"
                                         style="background-image: url('{{$ns->images}}');"></div>
                                </div>
                            </div>
                        </div>
                        <div class="news-item_content">
                            <div class="text text-s18">
                                <div class="con-box">
                                    {!! LC($ns->name) !!}
                                </div>
                            </div>
                            <div class="text text-s15">
                                {!! mb_substr(strip_tags(LC($ns->content)), 0, 200) !!}...
                            </div>
                            <p class="text text-s12">
                                {!! date("d m Y",strtotime($ns->day)) !!}
                            </p>
                        </div>
                    </a>
                @endforeach

            </div>

        </div>
    </section>

    <style>
        header.header {
            background-color: #013882;
            position: relative;
        }

        .contacts {
            padding-top: 0;
        }

        .slider:after {
            display: none;
        }
        .con-box {
            -webkit-column-width: inherit;
            -moz-column-width: inherit;
            column-width: inherit;
            overflow: inherit;
            height: inherit;
            height: inherit;
            /* text-overflow: ellipsis; */
            /* white-space: nowrap; */
            width: inherit;
            /* display: none; */
        }
        @media (max-width:575px) {
            .test-s12 {
                font-size: 0.96666667rem;
            }
        }
    </style>
@endsection
