@extends('views.layouts.app')

@section('content')


    <section class="vacancies">
        <div class="container vacancies-box">
            <div class="vacancies-box_text">
                <h4 class="after text text-roboto text-s32">
                    {!! LC($vac->name) !!}
                </h4>
                <div class="vacancies-content">
                    <div class="vacancies-content_main text text-roboto text-s16">
                        <p></p>
                        {!! LC($vac->content) !!}
                    </div>
                </div>
            </div>
            <div class="vacancies-box_list">
                <p class="text text-roboto text-s28" style="color: #014094;">
                    Открытые вакансии QazMedPro
                </p>
                <div class="vacanci-list">
                    @foreach($vacs as $vs)
                        <a href="{{url_custom('/vacancies/'.$vs->id)}}"
                           class="text text-roboto  text-s20">
                            {!! LC($vs->name) !!}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="vacans-bk">
        <div class="container">
            <h4 class="after text text-roboto text-s32">
                {!! s_("Заголовок","Форма 2","Откликнуться на вакансию","") !!}
            </h4>

            <form method="post" enctype="multipart/form-data" action="{{url_custom('/message')}}"
                  class="list_input">
                @csrf

                <input type="hidden" name="email-to" value="KZrecruitment@internationalsos.com">
                <input type="hidden" name="heading" value="Заявка на вакансию {{LC($vac->name)}}">
                <div class="input">
                    <div class="input_lab text text-roboto text-s16">
                        {!! s_("Номер телефона ","Форма 2","Контактный телефон","") !!}
                    </div>
                    <input class="input_in" required name="phone">
                </div>

                <div class="input">
                    <div class="input_lab text text-roboto text-s16">
                        {!! s_("Email ","Форма 2","E-mail","") !!}
                    </div>
                    <input class="input_in" type="email" name="email" required>
                </div>
                <input type="hidden" name="vacans" value="2">
                <div class="input" style=" padding: 1.5rem 0; padding-bottom: 0;">
                    <div class="input_lab file_box text text-roboto text-s18"
                         style=" display: flex; align-items: center; ">
                        <img src="/public/media/client/images/attach_file_24px.png" alt=""
                             style=" margin-right: 0.5rem; "> <span id="filename"
                                                                    style="text-decoration-line: underline;"> {!! s_("Кнопка Прикрепить резюме","Форма 2","Прикрепить резюме","") !!} </span>
                        <input type="file" name="files" onchange="window.sub(this)" required>
                    </div>
                </div>
{{--                <div style=" padding: 1.5rem 0; " class="g-recaptcha"--}}
{{--                     data-sitekey="6Le7s80ZAAAAAMUu_KDL2ElJ6zmSsBOiMx1U0ba2"></div>--}}
                <div class="input">
                    <button style="background: #014094;color:#fff;" type="submit"
                            class="input_in text text-roboto text-s18">{!! s_("Кнопка","Форма 2","Отправить","") !!}
                    </button>
                </div>
            </form>
        </div>
    </section>

    <style>
        header.header {
            background-color: #013882;
            position: relative;
        }

        .contacts {
            padding-top: 0;
        }
    </style>
@endsection
