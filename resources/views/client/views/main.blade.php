@extends('views.layouts.app')
@section('title')
{!!Strip_tags(s_("Title","Seo оптимизация","",""))!!}
@endsection

@section('description')
{!!  Strip_tags(s_("description","Seo оптимизация","",""))!!}
@endsection

@section('type')
website
@endsection

@section('ogimage')
https://qazmedpro.kz/graph.png
@endsection
@section('content')


    <section class="slider">
        <div class="prop">
            <div class="prop_img prop_img-50">
                <div class="prop_img_src">
                    <div class="b-text container">
                        <div class="b-text_main">
                            <h1 class="text title text-s45">
                                {!!  Strip_tags(s_("Первый банер Заголовок","Главная","<b> Медицинские услуги на промышленных и отдаленных проектах</b>","textarea"))!!}
                            </h1>
                            <div class="text text-s16">
                                {!! s_("Первый банер Описание","Главная","QazMedPro организовывает бесперебойную работу фельдшерских, врачебных медпунктов и амбулаторного лечения на промышленных объектах и вахтовых городках.","textarea") !!}
                            </div>
{{--                            <a href="{{url_custom('#')}}" class="btn btn-slid"><span--}}
{{--                                    class="text text-s16">Подробнее</span></a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="compani" class="mb-7">
        <div class="container">
            <div class="compan">
                <div class="compan_head">
                    <div class="compan_img">
                        <div class="prop_img prop_img-74">
                            <div class="prop_img_src" title="О компании"
                                 style="background-image: url('{!! s_("Второй блок картинка","Главная","/public/media/client/images/onas.png","images") !!}');">
                            </div>
                        </div>
                    </div>
                    <div class="compan_text">

                        <div class="text text-s16" style="font-weight: 900;color: #bad640;">
                            {!! s_("Второй блок доп Заголовок","Главная","<b> немного о нас</b>","textarea") !!}
                        </div>

                        <div class="title text text-s28" style="font-weight: 900;color: #2f3b4c;">
                            {!! s_("Второй блок основной Заголовок","Главная","О нашей компании","textarea") !!}
                        </div>
                        <div class="text text-s16 content">
                            {!! s_("Второй блок Описание","Главная","Основана в 2004 году, Ребрендирована в QazMedPro в 2019. Головной офис располагается в г. Алматы. <br> <br> Наша Миссия - организация медицинских пунктов и оказание догоспитальной квалифицированной медицинской помощи для защиты здоровья и жизни вашего персонала.","textarea") !!}
                        </div>

                        <a href="{{url_custom('/about')}}" class="btn btn-slid"><span class="text text-s16">{!! s_("Кнопка Читать больше","Кнопки","Читать больше","") !!}</span></a>

                    </div>
                </div>
                <div class="compan_foot">
                    <div class="compan_foot_img">
                        <div class="prop">
                            <div class="prop_img prop_img-65">
                                <div class="prop_img_src"
                                     style="background-image: url('{!! s_("Второй блок картинка 2","Главная","/public/media/client/images/icon/main.png","images") !!}');">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="compan_foot_text">
                        <div class="number-list">
                            <div class="number-list_head">
                                <p class="text text-s50">
                                     {!! s_("Второй блок плашка цифры первый","Главная","100+","") !!}
                                </p>
                            </div>
                            <div class="number-list_text">
                                <p class="text text-s14">
                                    {!! s_("Второй блок плашка цифры первые описание","Главная","Промышленных предприятий&nbsp;в&nbsp;РК","") !!}
                                </p>
                            </div>
                        </div>
                        <div class="number-list">
                            <div class="number-list_head active">
                                <p class="text text-s50">
                                      {!! s_("Второй блок плашка цифры вторые","Главная","250+","") !!}
                                </p>
                            </div>
                            <div class="number-list_text">
                                <p class="text text-s14">
                                    {!! s_("Второй блок плашка цифры вторые описание","Главная","квалифицированных медицинских работников","") !!}
                                </p>
                            </div>
                        </div>
                        <div class="number-list">
                            <div class="number-list_head">
                                <p class="text text-s50">
                                    {!! s_("Второй блок плашка цифры третьи","Главная","75+","") !!}
                                </p>
                            </div>
                            <div class="number-list_text">
                                <p class="text text-s14">
                                    {!! s_("Второй блок плашка цифры третьи описание","Главная","медицинских пунктов по&nbsp;всем&nbsp;регионам&nbsp;страны","") !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="services" class="services">
        <div class="services_head container">
            <h3 class="title text text-s14">
                {!! s_("Блок услуги доп заголовок","Главная","наши услгуи","") !!}
            </h3>
            <h4 class="after text text-s35">
                {!! s_("Блок услуги основной заголовок","Главная","Комплексное медицинское обслуживание","") !!}
            </h4>
        </div>
        <form id="submiDate" method="post" action="{{url_custom('/service')}}" class="container services-end">
            @php
                $allService=  \App\Service::orderby("sort")->count();
            $plus=1;
            @endphp
            <input id="count_current" name="count_current" type="hidden" value="8">
            <input id="count_all" name="count_all" type="hidden" value="{{$allService-8}}">
            <div class="services_main">

                @foreach(\App\Service::orderby("sort")->skip(0)->take(8)->get() as $v=>$service)
                    @include('views.singSerivese')
                @endforeach
            </div>
            @if($allService>8)
                <div class="services_btn">
                    <button type="submit" class="btn btn-slid"><span class="text text-s16">{!! s_("Кнопка Показать еще","Кнопки","Показать еще","") !!}</span></button>
                </div>
            @endif
        </form>
        <div id="video" class="container">
            <div class="services_video">
                <div class="prop">
                    <div class="prop_img prop_img-44">
                        <div class="prop_img_src"
                             style="background-image: url('/public/media/client/images/icon/video_bk.png')">

                            <div class="play_box">

                                <a href="javascript:void(0)"
                                   data-url="{!! s_("Видео ссылка youtube","Главная","https://www.youtube.com/embed/e9DrCjnItI0?autoplay=1&rel=0","") !!}"
                                   class="play_box_play">
                                    <div class="prop">
                                        <div class="prop_img prop_img-100">
                                            <div class="prop_img_src"
                                                 style="background-image: url('/public/media/client/images/play.png')">

                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="play_box_text">
                                    <div class="play_box_text_before">
                                        <p class="text text-s16">
                                            {!! s_("Видео заголовок","Главная","Видео презентация","") !!}
                                        </p>
                                    </div>
                                    <div class="play_box_text_name">
                                        <p class="text text-s35">
                                            {!! s_("Видео описание","Главная","Смотрите наши последние видео события","") !!}
                                        </p>
                                    </div>
                                </div>

                                <div class="play_box_video">
                                    <iframe id="video_fram"
                                            src=""
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "url": "https://qazmedpro.kz",
            "name": "QAZMEDPRO"
        }
        </script>

@endsection
