<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} :: Dashboard</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://qazmedpro.kz/public/media/admin/css/style.css?v=4">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-MJNV0WG02D"></script>

</head>
<body>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MJNV0WG02D');
</script>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<div class="container-fluid">
    <section class="mainsection row">
        <nav style="display: none;" class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand  mr-0" href="#">{{ config('app.name', 'Laravel') }}</a>
            <ul class="navbar-nav px-4">
                <li class="nav-item text-nowrap">
                    <a class="userExit" href="javascript:void(0);" style="color:#fff;" title="Выйти"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Выйти</a>
                </li>
            </ul>
        </nav>

        <nav class=" d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                @php
                    $model_urlink="";
                    if(isset($model_name)){
                                        $model_urlink=  $model_name;
                    }
                    $id_saving=-1;

                    if(isset($id)){
                        $id_saving=$id;
                    }

                    $dev_status=false;

                    if (Cache::has('dev')){
                        $dev_status=Cache::get('dev');
                    }

                @endphp
                <ul class="nav flex-column">
                    <li class="nav-item ">
                        <a href="{{url_custom("/admin")}}"
                           class="nav-link  "
                           data-closest=".nav-item">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-file-text">
                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                <polyline points="14 2 14 8 20 8"></polyline>
                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                <polyline points="10 9 9 9 8 9"></polyline>
                            </svg>
                            <span> Консоль</span>
                        </a>
                    </li>
                </ul>

                <ul class="nav flex-column">


                    @foreach(\App\Model_meta::where("type","table_catalog_availability")->where("name_key","1")->orderby("sort")->get() as $navlink)

                        <li class="nav-item {{$model_urlink==$navlink->attachment?"active":""}}">
                            <a href="javascript:void(0);"
                               class="nav-link {{$model_urlink==$navlink->attachment?"active":""}} cli" data-un=""
                               data-closest=".nav-item">
                                {!! ($navlink->model_name()->icon) !!}
                                <span> {{($navlink->model_name()->name)}}</span>
                            </a>

                            <ul class="nav nav-dropmenu flex-column">
                                <li class="nav-item ">
                                    <a href="{{url_custom("/admin/model/".$navlink->attachment)}}"
                                       class="nav-link {{$model_urlink==$navlink->attachment?($id_saving==-1?"active":""):""}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-layers">
                                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                            <polyline points="2 17 12 22 22 17"></polyline>
                                            <polyline points="2 12 12 17 22 12"></polyline>
                                        </svg>
                                        <span> Каталог</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url_custom('/admin/model/'.$navlink->attachment.'/0')}}"
                                       class="nav-link {{$model_urlink==$navlink->attachment?($id_saving==0?"active":""):""}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-plus-circle">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <line x1="12" y1="8" x2="12" y2="16"></line>
                                            <line x1="8" y1="12" x2="16" y2="12"></line>
                                        </svg>
                                        <span> Добавление записи</span>
                                    </a>
                                </li>
                            </ul>

                        </li>
                    @endforeach

                    <li class="nav-item ">
                        <a href="javascript:void(0);" class="nav-link  cli" data-un="" data-closest=".nav-item">
                            <span class="oi" data-glyph="rain"></span>
                            <span> Текста</span>
                        </a>

                        <ul class="nav nav-dropmenu flex-column">
                            <li class="nav-item ">
                                @php
                                    $thead_nav = \App\StaticText::get()->groupby("page");
                                @endphp
                                @foreach($thead_nav as $keyxs=>$the)
                                    <a href="{{url_custom('/admin/StaticText?page='.$keyxs)}}" class="nav-link ">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-layers">
                                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                            <polyline points="2 17 12 22 22 17"></polyline>
                                            <polyline points="2 12 12 17 22 12"></polyline>
                                        </svg>
                                        <span> {{$keyxs}}</span>
                                    </a>
                                @endforeach
                            </li>
                        </ul>

                    </li>
                </ul>


                @if($dev_status=="true")

                    <ul class="nav flex-column">


                        <li class="nav-item ">
                            <a href="javascript:void(0);"
                               class="nav-link cli" data-un=""
                               data-closest=".nav-item">
                                <span class="oi" data-glyph="cog"></span>
                                <span> Инструменты</span>
                            </a>

                            <ul class="nav nav-dropmenu flex-column">

                                <li class="nav-item ">
                                    <a href="{{url_custom("/admin/table")}}"
                                       class="nav-link {{$model_urlink=="table"?($id_saving==-1?"active":""):""}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-layers">
                                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                            <polyline points="2 17 12 22 22 17"></polyline>
                                            <polyline points="2 12 12 17 22 12"></polyline>
                                        </svg>
                                        <span> Таблицы</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{url_custom("/admin/column_name")}}"
                                       class="nav-link ">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-layers">
                                            <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                                            <polyline points="2 17 12 22 22 17"></polyline>
                                            <polyline points="2 12 12 17 22 12"></polyline>
                                        </svg>
                                        <span> Столбцы</span>
                                    </a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                @endif

                <a style=" position: absolute; bottom: 0; right: 0; padding: 1rem;color:#fff; "
                   href="{{url_custom("/admin/dev/".($dev_status?"false":"true"))}}">{{($dev_status?"debug: false":"debug: true")}}</a>
            </div>
        </nav>
        <main class="main  px-4" role="main">

            <div class="main_box">
                @yield('content')
            </div>

        </main>
    </section>
</div>
<script src="https://qazmedpro.kz/public/media/admin/js/app.js?v=4"></script>



<script>

    $(function () {
        <?php
        if(\Session::has('alert')){
        ?>
        $.fancybox.open('<?=Session::get('alert')?>');
        <?
        }
        ?>
    });

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(69054700, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/69054700" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
