@extends('views.layouts.app')

@section('content')

    <form method="post" action="{{url_custom("/admin/table_meta_update")}}">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
            <h1 class="h2">Изменение Таблицы</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group ">
                    <input type="submit" class="btn btn-sm  btn-success waves-effect  px-4" value="Применить">
                </div>
                <input type="hidden" name="model_name" value="{{$model_name }}">
            </div>
        </div>

        <div class="bodyMain">

            @csrf

            <div class="check_forms active">

                <div class="check_forms_body" style=" padding: 1rem;     padding-top: 1.5rem;">

                    {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название таблицы"],$tb_info)}}
                    {{maskInput($errors,["name"=>"icon_save","type"=>"text","placeholder"=>"Иконка"],$tb_info)}}
                    <p>
                        <a href="https://useiconic.com/open/" target="_blank">https://useiconic.com/open/</a> <b>Icon
                            Font</b>
                    </p>


                    <div class="language_grup" style="    margin-top: 22px;">
                        <label for="basic-url" class="label_control"> <b class="text_bigs">Показывать в меню</b> </label>

                        <div class="inputGroup-check " style=" padding-lefT: 0; padding-top: 0; ">
                            <input type="checkbox" name="availability" id="availability" value="1"
                                   {{$model_db->availability()->where("name_key","1")->count()>0?"checked":""}} class="chbox"
                                   style="display:none"/>
                            <label for="availability" class="toggle"><span></span>
                                <div class="text_box">ДА</div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>


            <div class="check_forms active" style="margin-top: 2rem;">
                <div class="check_forms_head">
                    <h3 class="head-sort">
                        Выводимые столбцы в каталог

                        <a href="javascript:void(null)" class="cli" data-closest=".check_forms">
                            <span class="oi" data-glyph="plus"></span>
                            <span class="oi" data-glyph="minus"></span>
                        </a>
                    </h3>

                </div>
                <div class="check_forms_body" style=" padding: 0 1rem;padding-bottom: 1rem; ">


                    <table
                        class="table sortTables  table-bordered table-striped js-table dataTable order-table no-footer">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Length</th>
                            <th class="chwidth">Not Null</th>
                            <th class="chwidth">Unsigned</th>
                            <th class="chwidth">Increment</th>
                            <th>Index</th>
                            <th>Default</th>
                            <th class="chwidth">Catalog</th>
                            <th class="chwidth">Edit</th>
                            <th class="chwidth">lang</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($columns as $keyx=>$colum)
                            <tr class="inputGroup-check ">
                                <td>
                                    {{maskInput($errors,["name"=>"","type"=>"text","attr"=>"select","placeholder"=>"none","value"=>$colum])}}
                                </td>
                                <td>
                                    <div class="row-fluid">
                                        <select class="selectpicker" name="lease_type_id_save" data-show-subtext="true"
                                                data-live-search="true">
                                            <option value=""></option>
                                            @foreach(get_type_sql() as $keyxs=>$type_sel)
                                                <optgroup label="{{$keyxs}}">
                                                    @foreach($type_sel as $ts)
                                                        <option value="{{$ts}}" data-subtext="">
                                                            {{$ts}}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    {{maskInput($errors,["name"=>"","type"=>"number","attr"=>"select","placeholder"=>"none","value"=>$colum])}}
                                </td>
                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" id="sfaw{{$keyx}}" name="sfaw{{$keyx}}"
                                               value="{{$colum}}"
                                               class="chbox"
                                               style="display:none"/>
                                        <label for="sfaw{{$keyx}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" id="saws{{$keyx}}" name="saws{{$keyx}}"
                                               value="{{$colum}}"
                                               class="chbox"
                                               style="display:none"/>
                                        <label for="saws{{$keyx}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" id="saw{{$keyx}}" name="saw{{$keyx}}" value="{{$colum}}"
                                               class="chbox"
                                               style="display:none"/>
                                        <label for="saw{{$keyx}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="row-fluid">
                                        <select class="selectpicker" name="lease_type_id_save" data-show-subtext="true"
                                                data-live-search="true">
                                            <option value=""></option>
                                            <option value="INDEX">INDEX</option>
                                            <option value="UNIQUE">UNIQUE</option>
                                            <option value="PRIMARY">PRIMARY</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    {{maskInput($errors,["name"=>"","type"=>"text","attr"=>"select","placeholder"=>"none","value"=>""])}}
                                </td>
                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" name="colum_list[]" id="radio{{$keyx+1}}"
                                               value="{{$colum}}"
                                               {{$model_db->meta("table_catalog")->where("name_key",$colum)->count()>0?"checked":""}} class="chbox"
                                               style="display:none"/>
                                        <label for="radio{{$keyx+1}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>

                                @php
                                    $model_colum=   $model_db->meta("table_save")->where("name_key",$colum)->first();
                                @endphp
                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" name="colum_save[{{$keyx}}][table_save]"
                                               value="{{$colum}}"
                                               id="colum_save{{$keyx+1}}"
                                               {{!is_null($model_colum)?"checked":""}} class="chbox"
                                               style="display:none"/>
                                        <label for="colum_save{{$keyx+1}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>

                                <td>
                                    <div class="inputchcon">
                                        <input type="checkbox" name="colum_save[{{$keyx}}][lang]" value="1"
                                               id="colum_save_lang{{$keyx+1}}"
                                               {{!is_null($model_colum)?($model_colum->languages=="1"?"checked":""):""}} class="chbox"
                                               style="display:none"/>
                                        <label for="colum_save_lang{{$keyx+1}}" class="toggle"><span></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>


        </div>
    </form>

    <style>
        .inputchcon {
            height: calc(1.5em + 0.75rem + 2px);
            display: flex;
            align-items: center;
            justify-content: center;
            width: 60px;
        }

        .chwidth {
            width: 60px;
            text-align: center;
        }
    </style>
@endsection
