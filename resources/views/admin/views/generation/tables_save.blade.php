@extends('views.layouts.app')

@section('content')

    <div>
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
            <h1 class="h2">{{GMN($model_name)}} : {{$id==0?"Добавление записи":"Обновление записи"}}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">

                <div class="langbtn"
                     style="margin-right: 2rem;padding-right: 2rem;border-right: 1px solid #000;display: flex;align-items: center;">
                    <p>
                        Выберите язык:
                    </p>
                    <?php
                    foreach (\App\Language::get() as $keyxs => $langs) {
                    ?>
                    <a href="javascript:void(0)"
                       data-lang="<?= $langs->name_key ?>"
                       class="btn btn-sm lang_control  btn-outline-success <?= $keyxs == 0 ? 'active' : ''?> waves-effect"
                       style="margin-left: 1rem;"><?= $langs->name ?></a>
                    <?php
                    }
                    ?>
                </div>

                <div class="btn-group ">
                    <a href="javascript:void(0);" onclick="$('#form_submit').submit()" type="submit"
                       class="btn btn-sm  btn-success waves-effect  px-4">Применить</a>
                </div>

            </div>
        </div>

        @if(View::exists('views.generation.input.'.$model_name.'._pre'))
            @include('views.generation.input.'.$model_name.'._pre')
        @endif

        <form id="form_submit" enctype="multipart/form-data" method="post" action="{{url_custom("/admin/update")}}"
              class="bodyMain">
            @csrf

            <input type="hidden" name="model_name" value="{{$model_name}}">
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="path" value="{{url_custom("/admin/model/".$model_name."/{id}")}}">
            @if(!is_null($model_db))
                @foreach($model_db->meta("table_save")->where("attachment",$model_name)->orderby("sort","desc")->get() as $modelSing)
                    @if($modelSing->name_key=="time")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"time","attr"=>"select","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="date" || $modelSing->name_key=="day")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"date","attr"=>"select","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="images" || $modelSing->name_key=="images_single")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"file","attr"=>"select","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}
                    @elseif($modelSing->name_key=="sites_name_key")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","attr"=>"select","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}
                    @else
                        @if($model_name=="Product" && $modelSing->name_key=="title")
                            {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"textarea","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}
                        @else
                            {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","placeholder"=>column_rename($modelSing->name_key),"lang"=>$modelSing->languages],$model)}}

                        @endif
                    @endif
                @endforeach
            @endif


            @if(View::exists('views.generation.input.'.$model_name.'._next'))
                @include('views.generation.input.'.$model_name.'._next')
            @endif

        </form>
    </div>

@endsection
