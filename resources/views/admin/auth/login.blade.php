@extends('auth.layouts.app')

@section('content')
<div class="bulpForm">

    <div class="bulpForm_body">
        <div class="bulpForm_body_head">
            <h2 class="msg">Вход в систему <b> {{ config('app.name') }}</b></h2>
        </div>
        <form id="sign_in" class="bulpForm_body_form" role="form" method="POST" action="{{ url_custom('/login') }}">
            {{ csrf_field() }}


            {{maskInput($errors,["name"=>"login","type"=>"text","placeholder"=>"Логин"])}}
            {{maskInput($errors,["name"=>"password","type"=>"password","placeholder"=>"Пароль"])}}


            <div class="autocheck" >
                <div class="form-check">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember">
                        <label class="custom-control-label" for="remember">Запомнить меня</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-sm btn-outline-secondary waves-effect px-4 ">Войти</button>
            </div>
            <div class="navlink" style="display: none;">
                <a href="{{ url('/admin/register') }}">Регистрация</a>
                <a href="{{ url('/admin/password/reset') }}">Забыли пароль?</a>
            </div>
        </form>
    </div>
</div>


@endsection
